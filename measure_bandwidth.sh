#!/bin/bash

function print_bandwidth()
{
	echo $1 | awk -F "\"*,\"*" '{printf "%.2f %s/s\n", $1/$4*1000000000, $2}'
}

temp_file=$(mktemp "${TMPDIR:-/tmp/}$(basename 0).XXXXXXXXXXXX")
APP="${@:-./kmscube}"
sudo perf stat -e uncore_imc/data_writes/,uncore_imc/data_reads/ -x, -a timeout 10 ${APP} > ${temp_file} 2>&1

reads=$(sed -n 1p ${temp_file})
writes=$(sed -n 2p ${temp_file})

echo -n "Read bandwidth: "
print_bandwidth "$reads"
echo -n "Write bandwidth: "
print_bandwidth "$writes"

rm ${temp_file}

#sudo intel_reg read 0x42404 && $(timeout 10 ./kmscube); sudo intel_reg read 0x42404
